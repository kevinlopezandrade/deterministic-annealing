import sklearn as skl
import sklearn.model_selection
import sklearn.datasets
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import scipy

import treelib as tl

from sklearn.utils.validation import check_is_fitted

# Avoid warnings
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)

def read_X_y_from_csv(sheet, y_names=None):
    """Parse a column data store into X, y arrays

    Args:
        sheet (str): Path to csv data sheet.
        y_names (list of str): List of column names used as labels.

    Returns:
        X (np.ndarray): Array with feature values from columns that are not contained in y_names (n_samples, n_features)
        y (dict of np.ndarray): Dictionary with keys y_names, each key contains an array (n_samples, 1)
                                with the label data from the corresponding column in sheet. 
    """

    # Read data frame
    data_frame = pd.read_csv(sheet)

    # Get columns not included in y_names
    columns = data_frame[data_frame.columns.difference(y_names)]

    # Convert to numpy array
    X = columns.values

    # Create dict with columns in y_names
    y = {}

    for col in y_names:
        column = data_frame[col].values
        
        # Reshape columns to (n_samples, 1)
        column.shape = (column.shape[0], 1)

        # Add columns to dicitonary
        y[col] = column


    return X, y


def compute_partition_function(x, yi_list, pi_list, T):
    """Computes the partition function for a given x sample"""

    normalization_coefficient_z = 0

    # for i in range(yi_list.shape[0]):
    for i in range(len(yi_list)):
        # Compute the term i of the sum
        term = pi_list[i] *  np.exp(-((np.linalg.norm(x - yi_list[i])**2)/T))
        normalization_coefficient_z = normalization_coefficient_z + term
    
    return normalization_coefficient_z


def compute_association_probability(y_i, x, yi_list, p_i, pi_list, metric, T):
    """Compute association probability for a  given x sample according 
       to mass constrained algorithm
       pi_list: farction of codevectors which represent effective cluster i"""

    normalization_coefficient = compute_partition_function(x, yi_list, pi_list, T)

    if metric == 'euclidian':
        numerator = p_i * np.exp(-((np.linalg.norm(x - y_i)**2)/T))
    else:
        pass

    association_probability = numerator/normalization_coefficient

    return association_probability



def compute_probability_cluster_yi(y_i, X_set, yi_list, p_i, pi_list, metric, T):
    """ Compute p(yi)"""

    n_samples = X_set.shape[0]

    cluster_probability = 0

    for i in range(n_samples):
        x = X_set[i]

        association_probability = compute_association_probability(y_i, x, yi_list, p_i, pi_list, metric, T)

        cluster_probability = cluster_probability + association_probability

    return (cluster_probability)*(1/n_samples)


def compute_centroid_y_and_cluster_probability(y_i, X_set, yi_list, p_i, pi_list, metric, T):
    """Compute centroid yi"""

    n_samples = X_set.shape[0]
    numerator = 0

    for sample_index in range(n_samples):
        x = X_set[sample_index]
        association_probability = compute_association_probability(y_i, x, yi_list, p_i, pi_list, metric, T) 
        term = x * association_probability * (1/n_samples) # Maybe I have to change because of numerically stability
        
        if term.size != X_set.shape[1]:
            raise ValueError("Shape is not correct for the vector")

        numerator = numerator + term

    cluster_probability = compute_probability_cluster_yi(y_i, X_set, yi_list, p_i, pi_list, metric, T)

    centroid = numerator/cluster_probability

    if centroid.size != X_set.shape[1]:
        raise ValueError("Centroid doesn't have a correct shape")

    return centroid, cluster_probability


def compute_covariance_matrix(X_set, y_i, yi_list, p_i, pi_list, metric, T):
    """X_set: array with each datum x (samples, n_features)
       y_i: vector centroid of cluster i to which we compute the covariance matrix
    """

    n_samples = X_set.shape[0]
    n_features = X_set.shape[1]

    covariance_matrix = np.zeros((n_features, n_features))

    for row_index in range(n_samples):
        x_vector = X_set[row_index,:]

        # Force to be a vector instead of array
        x_vector.shape = (x_vector.shape[0], 1)

        # Compute outer product of the difference (x-y)*(x-y)'
        x_covariance_matrix = np.dot(x_vector - y_i, (x_vector - y_i).T)


        # VIP: We compute the association probability with the old value for the p_i
        association_probability = compute_association_probability(y_i, x_vector, yi_list, p_i, pi_list, metric, T)

        posterior_probability = (association_probability*(1/n_samples))/(p_i)

        term = posterior_probability * x_covariance_matrix

        # Add to local covariance matrix to the total covariance matrix
        covariance_matrix = covariance_matrix + term

    return covariance_matrix


def compute_initial_centroid(X_set):

    n_features = X_set.shape[1]
    n_samples = X_set.shape[0]

    y = np.empty((n_features, 1))

    for sample_index in range(n_samples):
        x = X_set[sample_index]

        # Ensure that shape is (n_features, 1)
        x.shape = (n_features, 1)

        # Add all the samples to compute the centroid
        y = y + x


    return (y/n_samples)

    
def compute_initial_covariance_matrix(X_set):
    """X_set: array with each datum x (samples, n_features)
    """

    n_samples = X_set.shape[0]
    n_features = X_set.shape[1]

    covariance_matrix = np.zeros((n_features, n_features))

    for row_index in range(n_samples):
        x_vector = X_set[row_index,:]

        # Force to be a vector instead of array
        x_vector.shape = (x_vector.shape[0], 1)

        # Compute outer product of the difference
        x_covariance_matrix = np.dot(x_vector, x_vector.T)

        term = x_covariance_matrix * (1/n_samples)

        # Add to local covariance matrix to the total covariance matrix
        covariance_matrix = covariance_matrix + term


    if covariance_matrix.shape != (n_features, n_features):
        raise ValueError("Covariance matrix doesn't have correct shape")

    return covariance_matrix

def compute_cluster_principal_component(covariance_matrix):
    """Compute the critical temperature"""

    eigvals = scipy.linalg.eigvals(covariance_matrix)
    
    cluster_principal_component = np.sort(np.abs(eigvals))[-1]

    return cluster_principal_component


def check_condition_phase_transition(covariance_matrix, T):

    eigvals = scipy.linalg.eigvals(covariance_matrix)
    cluster_principal_component = compute_cluster_principal_component(covariance_matrix)

    if T < 2*cluster_principal_component:
        # T has reached the T_critical for cluster associated with the covariance_matrix
        return True
    else:
        return False


def convergence_test(yi_list, new_yi_list):
    pass

def cooling_step(T, alfa):

    if alfa >= 1:
        raise ValueError("Alfa should be less than 1")

    T = alfa*T

    return T

class DeterministicAnnealingClustering(skl.base.BaseEstimator, skl.base.TransformerMixin):
    """Template class for DAC
    
    Attributes:
        cluster_centers_ (np.ndarray): Cluster centroids y_i (n_clusters, n_features)
        cluster_probabs_ (np.ndarray): Assignment probability vectors p(y_i | x) for each sample
                                       (n_samples, n_clusters)
        bifurcation_tree_ (treelib.Tree): Tree object that contains information about cluster evolution during
                                          annealing.
                                       
    Parameters:
        n_clusters (int): Maximum number of clusters returned by DAC.
        random_state (int): Random seed.
    """
    
    def __init__(self, n_clusters=8, random_state=42, metric="euclidian", T_min=0.1):
        self.n_clusters = n_clusters
        self.random_state = random_state
        self.metric = metric
        self.T_min = T_min
        self.last_T = None
        self.cluster_probabilities = None
        self.cluster_probabs_ = None
        # Add more parameters, if necessary.
    
    def fit(self, X):
        """Compute DAC for input vectors X
       
        Preferred implementation of DAC as described in reference [1].
        Consider to use initialization and reseeding as in sklearn k-means for improved performance.
        
        Args:
            X (np.ndarray): Input array with shape (samples, n_features)
        
        Returns:
            self
        """

        Cx = compute_initial_covariance_matrix(X)
        T = 2 * compute_cluster_principal_component(Cx) + 0.1 # Should be greater than 2*max(eigenvals(Cx))

        # Create the tree to track the bifurcations
        tree_clusters = tl.Tree()

        # Compute the initial centroid
        y1 = compute_initial_centroid(X)

        # Initialize the lists
        yi_list = [y1]
        pi_list = [1]


        # Min temperature condition
        above_min_temperatue = True
        is_last_iteration = False

        while above_min_temperatue:


            # for i in range(yi_list.shape[0]):
            for i in range(len(yi_list)):

                y_i = yi_list[i]
                p_i = pi_list[i]
                
                new_yi, new_pi = compute_centroid_y_and_cluster_probability(y_i, X, yi_list, p_i, pi_list, self.metric, T)

                yi_list[i] = new_yi
                pi_list[i] = new_pi

            if is_last_iteration:
                break
            
            if T < self.T_min:
                is_last_iteration = True
                continue

            T = cooling_step(T, 0.95)
            print(T)

            # if yi_list.shape[0] < self.n_clusters:
            if len(yi_list) < self.n_clusters:
                # for cluster_index in range(yi_list.shape[0]):
                for cluster_index in range(len(yi_list)):
                    cluster_centroid = yi_list[cluster_index]
                    cluster_pi = pi_list[cluster_index]

                    local_covariance_matrix = compute_covariance_matrix(X, cluster_centroid, yi_list, cluster_pi, pi_list, self.metric, T)
                    reached_critical = check_condition_phase_transition(local_covariance_matrix, T)

                    # Add random noise to the new cluster centroid
                    if reached_critical:
                        print("Added new cluster")
                        new_cluster_centroid = cluster_centroid + np.random.normal(0, 0.2, size=len(cluster_centroid))
                        new_cluster_probability = cluster_pi/2

                        # Update also current cluster probability
                        cluster_pi_updated = cluster_pi/2

                        # Append the new cluster centroid
                        yi_list.append(new_cluster_centroid)

                        # Update and append the pi_list
                        pi_list[cluster_index] = cluster_pi_updated
                        pi_list.append(new_cluster_probability)
                        break # In theory we can just add one cluster not two more

            
        self.cluster_centers_ = np.array(yi_list)
        self.cluster_probabilities = np.array(pi_list)
        self.last_T = T

        return self

    def predict(self, X):
        """Predict assignment probability vectors for each sample in X.

        Args:
            X (np.ndarray): Input array with shape (new_samples, n_features)

        Returns:
            P (np.ndarray): Assignment probability vectors (new_samples, n_clusters) 
        """

        n_samples = X.shape[0]

        P = np.empty(n_samples, self.cluster_centers_.shape[0])

        yi_list = self.cluster_centers_.tolist()
        pi_list = self.cluster_probabilities.tolist()


        for sample_index in range(n_samples):
            x = X[sample_index]

            assignment_probabilty_vector_for_x = []

            for cluster_index in range(self.cluster_centers_.shape[0]):
                assignment_probabilty = compute_association_probability(yi_list[cluster_index], x, yi_list, pi_list[cluster_index], pi_list, self.metric, self.last_T)

                assignment_probabilty_vector_for_x.append(assignment_probabilty) 

            P[sample_index] = assignment_probabilty_vector_for_x


        return P
    
def test_dac():
    n_samples = 300

    # generate random sample, two components
    np.random.seed(0)

    # generate spherical data centered on (20, 20)
    shifted_gaussian = np.random.randn(n_samples, 2) + np.array([20, 20])

    # generate zero centered stretched Gaussian data
    C = np.array([[0., -0.7], [3.5, .7]])
    stretched_gaussian = np.dot(np.random.randn(n_samples, 2), C)

    # concatenate the two datasets into the final training set
    X_train = np.vstack([shifted_gaussian, stretched_gaussian])

    DAC = DeterministicAnnealingClustering(n_clusters=2, T_min=0.17)
    DAC.fit(X_train)

    cluster_centers = DAC.cluster_centers_
    
    plt.scatter(X_train[:,0], X_train[:,1])
    plt.scatter(cluster_centers[:,0], cluster_centers[:,1])
    plt.show()
