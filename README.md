# Deterministic Annealing

This repo contains an implementation of the **Deterministic Annealing**
algorithm used to perform clustering following the principle of maximum
entropy. The algorithm is applied to a wine dataset and the implementation
follows the **Sklearn** API.

#### References

* Deterministic annealing for clustering, compression, classification,
  regression, and related optimization problems, Kenneth Rose, 1998,
  http://ieeexplore.ieee.org/document/726788/

* The wine data set, http://www3.dsi.uminho.pt/pcortez/wine5.pdf 
